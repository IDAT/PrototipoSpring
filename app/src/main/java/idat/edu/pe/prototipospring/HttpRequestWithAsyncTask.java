package idat.edu.pe.prototipospring;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

public class HttpRequestWithAsyncTask extends AsyncTask<String,Void,JSONObject> {

    Context context;
    ProgressDialog dialog;

    public HttpRequestWithAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {

        //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
        dialog = new ProgressDialog(context);
        dialog.setTitle("Aviso");
        dialog.setMessage("Cargando Usuarios...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(false);
        dialog.show();

    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
    }
}
