package idat.edu.pe.prototipospring.controller;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.HashMap;

public class UsuarioController {

    final static String strUrlRest = "http://10.0.2.2:8080/inventarios/Usuario/Rest";
    //final static String strUrlRest = "http://10.0.2.2:80/slim2/Usuario";
    HttpHeaders headers = null;
    RestTemplate restTemplate = null;
    JSONObject jsonObject = null;

    public UsuarioController() {
        //TODO - CONFIGURAR HEADER
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        //TODO - CONSUMO DEL SERVICIO REST + CONVERSION
        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
    }

    public JSONObject selUsuarioAll(){
        try {
            /*
                String strResult = restTemplate.getForObject(strUrlRest, String.class);
                JSONResult = new JSONObject(strResult);
            */
            ResponseEntity<String> responseEntity = restTemplate.exchange(strUrlRest, HttpMethod.GET, null, String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                jsonObject = new JSONObject(responseEntity.getBody());
            }
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject delUsuario(String codigo){
        try {
            /*
                Map<String, String> params = new HashMap<String, String>();
                params.put("codigo", "USU-007");
                restTemplate.delete( argumento[0],  params );
            */
            ResponseEntity<String>  responseEntity = restTemplate.exchange(strUrlRest + "/" + codigo, HttpMethod.DELETE, null, String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                jsonObject = new JSONObject(responseEntity.getBody());
            }
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject updUsuario(JSONObject JSONRequest){
        try {
            HttpEntity<String> entity = new HttpEntity<String>(JSONRequest.toString(), headers);
            ResponseEntity<String>  responseEntity = restTemplate.exchange(strUrlRest, HttpMethod.PUT, entity, String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                jsonObject = new JSONObject(responseEntity.getBody());
            }
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject insUsuario(JSONObject JSONRequest){
        try {
            HttpEntity<String> entity = new HttpEntity<String>(JSONRequest.toString(), headers);
            ResponseEntity<String>  responseEntity = restTemplate.exchange(strUrlRest, HttpMethod.POST, entity, String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                jsonObject = new JSONObject(responseEntity.getBody());
            }
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

}
