package idat.edu.pe.prototipospring;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import idat.edu.pe.prototipospring.adapter.UsuarioAdapter;
import idat.edu.pe.prototipospring.bean.UsuarioBean;
import idat.edu.pe.prototipospring.controller.UsuarioController;

public class UsuarioActivity extends AppCompatActivity {
    // DECLARACION DE VARIABLE PUBLICA
    ListView lv;
    UsuarioController usuarioController = new UsuarioController();
    View coordinatorLayoutView;
    UsuarioAdapter usuarioAdapter;
    ArrayList<UsuarioBean> aryUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        // ENLAZAR OBJECT XML A OBJECT JAVA
        coordinatorLayoutView = findViewById(R.id.MyCoordinatorLayout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lv = findViewById(R.id.lstUsuarios);
        FloatingActionButton fab = findViewById(R.id.fab);

        // FAB EVENTO CLIC
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO - LLAMAR A LA TAREA ASYNCTASK
                new HttpRequestTask().execute();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                UsuarioBean u = usuarioAdapter.getItem(position);
                Intent x = new Intent(UsuarioActivity.this,UsuarioNewActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("codigo",u.getUsu_codigo());
                bundle.putString("nombre",u.getUsu_nombre());
                bundle.putString("passwd",u.getUsu_passwd());
                bundle.putString("descri",u.getUsu_descri());
                bundle.putString("email",u.getUsu_email());
                bundle.putInt("estcod",u.getUsu_estcod());
                x.putExtras(bundle);

                startActivity(x);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_nuevo) {

            Intent x = new Intent(UsuarioActivity.this,UsuarioNewActivity.class);
            startActivity(x);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //TODO - LLAMAR A LA TAREA ASYNCTASK
        new HttpRequestTask().execute();
    }

    private class HttpRequestTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(UsuarioActivity.this);
            dialog.setTitle("Aviso");
            dialog.setMessage("Cargando Usuarios...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... argumento) {
            return usuarioController.selUsuarioAll();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                //TODO - CERRAR PROGRESSDIALOG SI ESTA VISIBLE
                if(dialog.isShowing())dialog.dismiss();
                //TODO - MOSTRAR KEY MESSAGE
                Snackbar.make(coordinatorLayoutView, jsonObject.getString("message") ,Snackbar.LENGTH_LONG).show();
                if(jsonObject.getBoolean("status") && jsonObject.has("data")) {
                    //TODO - OBTENER JSONARRAY DEL JSONOBJECT
                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    //String[] values = new String[jsonArray.length()];
                    aryUsuarios = new ArrayList<>();

                    for (int x = 0; x < jsonArray.length(); x++) {
                        //TODO - OBTENER JSONOBJECT DEL JSONARRAY
                        JSONObject i = jsonArray.getJSONObject(x);
                        //values[x] = i.getString("usu_descri"); //KEY
                        UsuarioBean u = new UsuarioBean();
                        u.setUsu_codigo(i.getString("usu_codigo"));
                        u.setUsu_nombre(i.getString("usu_nombre"));
                        u.setUsu_passwd(i.getString("usu_passwd"));
                        u.setUsu_descri(i.getString("usu_descri"));
                        u.setUsu_email(i.getString("usu_email"));
                        //u.setUsu_fecreg(new Date(i.getString("usu_fecreg")));
                        aryUsuarios.add(u);
                    }

                    //TODO - CREAR ARRAYADAPTER
                    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1, values);
                    usuarioAdapter = new UsuarioAdapter(UsuarioActivity.this, R.layout.layout_usuario, aryUsuarios);
                    usuarioAdapter.notifyDataSetChanged();

                    //TODO - CARGAR ARRAYADAPTER A LISTVIEW
                    lv.setAdapter(usuarioAdapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}


