package idat.edu.pe.prototipospring;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import org.json.JSONObject;

import idat.edu.pe.prototipospring.controller.UsuarioController;

public class UsuarioNewActivity extends AppCompatActivity {
    EditText edtCod, edtNom, edtPwd, edtDes, edtEma;
    UsuarioController usuarioController = new UsuarioController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_new);

        // ENLAZAR OBJECT XML A OBJECT JAVA
        //linearLayoutView = findViewById(R.id.MyLinearLayout);
        edtCod = findViewById(R.id.edtCodigo);
        edtNom = findViewById(R.id.edtNombre);
        edtPwd = findViewById(R.id.edtPasswd);
        edtDes = findViewById(R.id.edtDescri);
        edtEma = findViewById(R.id.edtEmail);
        edtCod.setEnabled(false);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null ) {
            edtCod.setText(bundle.getString("codigo"));
            edtNom.setText(bundle.getString("nombre"));
            edtPwd.setText(bundle.getString("passwd"));
            edtDes.setText(bundle.getString("descri"));
            edtEma.setText(bundle.getString("email"));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_usuario, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_eliminar ){

            return true;
        }

        if (id == R.id.action_guardar){
            //TODO - CREAR REQUEST JSON
            /*
            JSONObject JSONRequest = new JSONObject();
            JSONRequest.put("usu_codigo", null);
            JSONRequest.put("usu_nombre","AOCAÑA");
            JSONRequest.put("usu_descri","ASTRID OCAÑA");
            JSONRequest.put("usu_passwd","123");
            JSONRequest.put("usu_email","@");
            JSONRequest.put("usu_imagen",null);
            JSONRequest.put("usu_fecreg","2018-01-02 00:00:00");
            JSONRequest.put("usu_estcod",1);
            JSONRequest.put("usu_estdes",null);
            */
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
