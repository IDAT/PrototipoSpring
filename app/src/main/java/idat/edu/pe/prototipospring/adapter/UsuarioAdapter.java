package idat.edu.pe.prototipospring.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import idat.edu.pe.prototipospring.R;
import idat.edu.pe.prototipospring.bean.UsuarioBean;

public class UsuarioAdapter extends ArrayAdapter<UsuarioBean>{

    int resource;
    ArrayList<UsuarioBean> data;

    public UsuarioAdapter(@NonNull Context context, int resource, @NonNull ArrayList<UsuarioBean> data) {
        super(context, resource, data);
        this.resource = resource;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder holder;

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource,parent,false);

            holder = new ViewHolder();
            holder.avatar = (ImageView) convertView.findViewById(R.id.igvAvatar);
            holder.nombre = (TextView) convertView.findViewById(R.id.txvNombre);
            holder.descri = (TextView) convertView.findViewById(R.id.txvDescri);
            holder.estado = (TextView) convertView.findViewById(R.id.txvEstado);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        UsuarioBean u =getItem(position);
        holder.nombre.setText(u.getUsu_nombre());
        holder.descri.setText(u.getUsu_descri());
        //holder.estado.setText(u.getUsu_fecreg().toString());
        return convertView;
    }

    static class ViewHolder {
        ImageView avatar;
        TextView nombre;
        TextView descri;
        TextView estado;
    }
}
